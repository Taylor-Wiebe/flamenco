// Package api provides primitives to interact with the openapi HTTP API.
//
// Code generated by github.com/deepmap/oapi-codegen version v1.9.0 DO NOT EDIT.
package api

import (
	"bytes"
	"compress/gzip"
	"encoding/base64"
	"fmt"
	"net/url"
	"path"
	"strings"

	"github.com/getkin/kin-openapi/openapi3"
)

// Base64 encoded, gzipped, json marshaled Swagger object
var swaggerSpec = []string{

	"H4sIAAAAAAAC/+y923LcOJYo+iuInBPhqpjMlCz5Ula/HLcvVaq2yxpL7jonWhVKJInMhEUCbAJUOtvh",
	"iPmI8ydnT8R+2PO0f6Dmj3ZgLQAESTAvsiWr3NMP1VaSxGVhYd0vHweJzAspmNBqcPRxoJIFyyn886lS",
	"fC5YekbVpfk7ZSopeaG5FIOjxlPCFaFEm39RRbg2f5csYfyKpWS6InrByK+yvGTleDAcFKUsWKk5g1kS",
	"medUpPBvrlkO//i/SjYbHA3+Za9e3J5d2d4z/GDwaTjQq4INjga0LOnK/P1eTs3X9melSy7m9veLouSy",
	"5HoVvMCFZnNWujfw18jngubxB+vHVJrqauN2DPxO8U2zI6ou+xdSVTw1D2ayzKkeHOEPw/aLn4aDkv29",
	"4iVLB0d/cy8Z4Ni9+LUFW2hBKQBJuKphfV6/+Xnl9D1LtFng0yvKMzrN2M9yesq0NsvpYM4pF/OMEYXP",
	"iZwRSn6WU2JGUxEEWUie4D+b4/y6YILM+RUTQ5LxnGvAsyua8dT8t2KKaGl+U4zYQcbkjchWpFJmjWTJ",
	"9YIg0GByM7dHwQ7w28iWshmtMt1d19mCEfsQ10HUQi6FXQypFCvJ0qw9ZZqVORcw/4IrB5IxDh+MGZ/C",
	"/7Knpcw0L+xEXNQTGXwsZzRhMChLuTZbxxHt+mc0U2zYBa5esNIsmmaZXBLzaXuhhM60eWfByHs5JQuq",
	"yJQxQVQ1zbnWLB2TX2WVpYTnRbYiKcsYfpZlhH3gCgek6lKRmSxx6PdyOiRUpIaAyLzgmXmH6/G5qBF9",
	"KmXGqIAdXdGsC5+TlV5IQdiHomRKcQnAnzJi3q6oZqmBkSxT3KA7BwY7aR6dX5c/m2EXNcywx2Imuwt5",
	"zTQdpVRTOxAj98zL94KldTG+c/T2oAaD9ik9r/8y92i5oDo+iaHIqTTrJ8dAnmmmpMGQ1FDsIqMJW8gM",
	"4ME+aAMUg0qIpmbAnIqKZoSLotJkxpk5U0UWPE2ZIN9NWUIrheAdSTHC86/xQcv5PGMpkcJxA4Ob3zfO",
	"tIammfkVF5d/rrRuQSCKqi+EQWlVb9zMg0u4Z6cmUxiLTNmCXnFZdo+VPG29uuRZZlDGX6k/Z0ykrLyn",
	"cGwLVn+9CJCjeqdDWM/ErGcSHgSM28Q4u4Z7CnFuTF4DtLNVcOlqeslhp4IISTIp5qwkhVSKTzOG94YL",
	"pRlNga6K8MRwRfcC4N1z1M8AwuxzfC6emmtD8yKDQ7KzES1HUzYqAQIsJbOS5oyUVMzZkCwXPFmYg3U3",
	"h1Za5lTzBPYwk4Z+4DAqYcJ/N600Sag5FCKvWFkiMuVu75ZEKsPG4re/xedaeNNEkxi3umSr7o09TpnQ",
	"fMZZ6a+shfyQ5JXSZrmV4H+vkH9YWvve8q8oeTC3m5bzCAt7KlaEfdAlJbScV7kRDBybmBarsflQjU9l",
	"zk6QQKy++54YqOLN1ZIkJaOaISpbIrIK1lDvtQbUDpSf5zlLOdUsW5GSmaEIha2mbMYFNx8MDZ7B9GbK",
	"IcBEVtquiJaaJ1VGS3/Pesi4qqZO6lknLEXki1P7pefQO49wZj+/4nCLrjHCX82XPDNyUxspDY7ZlW0p",
	"MJ3WoGjJTdV0ZJ4gxBHnPPl6VpUlEzpbEWkkHOrGBSQOZBw1JpOfnp7+9OL5xcvjVy8uTp6e/TRB+T3l",
	"JUu0LFekoHpB/pVMzgd7/wL/Ox9MCC0Kc/3tXWSiys3+ZjxjF+Z9c9946f4JP1tZc0HVgqUX9Zu/Re5I",
	"37l0RR8LgWD3wcVEwY4qcvzcXRnYdkDAx+QXSQRTRgpQuqwSXZVMke9AsFNDkvLETEVLztT3hJaMqKoo",
	"ZKnbW7eLHxqZ//DAbDqTVA+GgNfbbjJAnQard8g4jAm9jj03OdjEfjM5IjRb0hXS9DGZ1PxqcoToAV9b",
	"0vXuGEVwAKgV3EryXcYvGaEOaISm6UiK78dksmTT2DBLNq25IWBdTgWdM0PUkNYLqZGo21kcY3svp2My",
	"QVlickQEu2IlDP2nNi5b0mhWirKheRGAA3qnmV3QrElr3GnVAMWZBkB0LFwGw8GSTTeeWRwjne5S4wlK",
	"OVwZRk7nrLSMWQNFpLlh/hFFh2ka0ZZ+omoR3njgMuS4QwIUsdwqo1OWkWSBTBaWYUZGwQN/HpMz8zNX",
	"yEekqA/fS8tMqKo0nMWKlF6mb05q7kdVgBRNNeuR6GBJu6nWboKtzQIx1bOjtbWIsyVQuLxgziGexSaC",
	"bdAhwtRfcaUdhQKS248YXSRwWvf1Nn7W4IQ9u66niG3QXvgTqhfPFiy5fMuU1XJbarmR+Lub72gkKycK",
	"6IVBuO+E1N9bOh0VlkBgjWu8KMsCRi6pQtXfYN6MixRncSQ+OrC6wGmjlgQUeRbML9SyElkaujWOCi3A",
	"zKIrhUH8QmeyEml0TUpWZbJR4giO5BQ/aB8pAs2uyA8b7nloD2zDkb/kIq1PfCv860GYiMWkuw9D9UJB",
	"giolE041kmSzmwsmrq5oObCI0S9AOLNg5zzsA1Iyo4OBiE2JQhuUNWYBvfvAkkqzTebKflugp+zBYwfj",
	"ON0JPokdy4uylGV3Pz8ywUqeEGYek5KpQgrFYobVNILqP52dnRC0/hHzhhff/UDk2LDSJKtSNJPgpVhl",
	"kqZEScRqD0BcbQO2WWaXxgXaKbk0euUzM9nD/UPPdbxtIaWaTinqmtNKrQx3YgQW6hZlmZcUmnJBKLn3",
	"lulyNXo606y8h68uGAXzhVkeFylPqGbKGqhQQ9U8R33bHAVTXvksmS45S8fkJWiqTiyxA3IFgotBE2qE",
	"Y8fL7ynL98y7ScaZALNJKomSOTOK4ZyUjCoJ1gkC4hT7gJeH04xMaXIpZzPkmN6g60TJrjU5Z0rReQz3",
	"WsgF516/H8WsKyb0S1rmp1uZqOs33zLDx/wQP8vpu8Lw/aiyopj2xt0hMdgBej45lckl08dv9l7/29kZ",
	"ogFKnyicKHMQJRFsaX5UQzIpSnbFZaUuEG8n3jbDPiCaIhDbRrSMaXZhz5qlFzTCVY5nVp3NGHAsQ639",
	"F1Z4chYQnjOlaV4QQ9URoQyuOWQynyotS5SnXmY0ZyKRntE3j9nAbGRGjDKqCBF79+74uZMCfwZD/gYf",
	"QC1aNQf6heahAhk3VzTAvQk7jLzl/RehR8QrMw/3YwhdslnJ1OIC7L+Ro/F32Iug9papBdiU7fdAcOxu",
	"7im0JtfyLWAdKiPKXFgDeDU0SAdya0pBC2E0WQDRuOJpRTP0ZC1hlrmhtmBikdIQgZUbxFqUi5ImYOnq",
	"tWzsDsR+/w9MHUGPM4+cckYyqrRd5dY4t6TqAm9M2uNowStqsPy9Ubbty/UdMbddSzLRZcUmVkGxTwqW",
	"8Bk3L4M+B1ZInt6r7ciK6aGlzOYmududF3q1leUPLoADTuDcsi6rwKnVRLpe2viKKv3WGjv7KJxFUFnW",
	"CGogXxtJeU7nNX910LPLjEv+W7n3hgO9qPKpoDzbAq3CrRybFYGjIqYT4FxUXdp/+Un6wcRn7NkqiYnU",
	"ngBmfMZGiXmJsCuwBVjbu9EegSuqRYXGgFQuxdAIJyX8WRVDwnQSI+7bWPr84mCpqBm1dt1rlsNPqLp8",
	"Jed95w+O70zOSbKoxKVlcFoSSoCvaVnwZM/xOlJKmZOUIU1L8T0rQxmQD+GXK8lTM04KMkiL4MTgkMmI",
	"xeCZWY+j8dquckxe05WXoPIq07wAsUQwBe+yDzqqojiEWMuSIERguKNfukY1s421x7CNlHEGYNwgZgA4",
	"OnIGUIPrChqG/l81gwC25+XbAW64C3HYzPc1Tvq5jL8ZuXCdb26Kn8XYg6dwVvmKsAt/kr24iFrhGe0l",
	"CvgCOaPzDajItUfDGH1DS+A6SPqlbMu+wQa4JfvezHL77GMBmLa5tPjmxmu7RLCugVhCxYWRHmip19l3",
	"uLJTgvJHKy1H9qu4icfCKao8OBkTTeFM1xqtXa6Bth1g/MWkf1z+NjTD3JsLxVgkfsQIBU4f5ipcr3nf",
	"2UACI+V2a99MepZu9Z9LfBAMu5Kf+FcXiFe7fPwMvniLut/NiuZXrFQ2EmQLMtdP3dw4w8Zdid3hpmXA",
	"GeiAOoJRMQV74pJCbIKhmypjrAATnbmS1L5XiUshlwLXACJd1HDXsS6YOTECAQIS7UJw2k/te692tGB0",
	"owbw5ygcrAz71/oEgoXNOfjpDscHo8ePRvM0PXyQPjz8wZ3B0eD/lVXp7tAAwlpK7Q9zcDg+HNGsWND9",
	"4GzCn8l3nbG/7+4fVrGTY6WxjI9r8a2JyRYMXqPxzq2cUatlL6qcCiNlqiqHz1DGKlnGqGJkWvEsdQGi",
	"4FQypIEqMglXNUEVQQLJrj+BiCVrmMSvJ3OuJ8R+BebGqP+pdeD1PWiAwl8dA9EYNvyMwaU0y97MBkd/",
	"W49wp85bZr76NPy4RmZc6z9xWiVxXxApvD4ZldcxIiRmBzcPwLnnKNLWJOif3pZ2DSPOzgxh/BnCrTv0",
	"DWLtp98Qj/+cyeQy40r3Oy+RUVvjGy0ZGMEhEpSlJGElqJGgTaGLUxoxzVp6EoecW/mPwvW8ELpcxVxH",
	"3Zc6Dsn1odO4n211KPt2DxFtnUA9dBgp3UNCntvrEQ8XNb8SOpWVxlhOp39aKdJJmNacxBviZYsvLmhO",
	"xUWyYMmlrPR6n+cpvEzcy0EkkFtAyXJ5xVJCMynmGDjtQje2CcxrrqUHNHFLVWfhL4Ss5ovQuwTsggZO",
	"mIKzhBEt57jFlM9mrATTMZwg2G7N14SShQSTXQZCC3n39pVz6URseWNyJoG5QdQQBs+8fTU0PyVUM0E1",
	"I+eDj1Oq2Ke9j1J4qVdVsxn/wNSn80FMdzEfNNGyzKJUyA7TcM1uiFNvHQVMFYzUcxSvqVIOU09ZxpJ4",
	"GPqJd2BiGLV5NmWWor+XU+Vs9TUKG3QJhCjQUSzNusjph8HR4GD/4HC0/2i0f//s/uHR/QdH9x/+6/7B",
	"0f5+V/jpft0JsMwyXAg641nJQpJrFjaTJXj5HV+teVPr8u1An6MgZZqmVFNg/2kKwZM0O4mYNRuMt7GZ",
	"csp1ScsVye1gDqHH5LXZhqGuGfsQhrVZH2cuzS4g/qRSXMzJhI6n42RiyHp9hwyuXrJV64yKUsI+jgan",
	"Rck1Iy9LPl9ow2wUK8csB0P0QK2mJRP/99SGYMhy7t6w8vApvEBO9f/+X1csG/TA6cQa6595nax55qGH",
	"KacfeG60k/v7+8NBzgX+FXE3ta6BH6QH/0+D6KP4YemyYj3f9mtOCRWJOQZMoynQXjMczCjHHwtaKfjH",
	"3ytW4WvwxcjLUQPcB6sYql6VgfXI06RmpHONR35ZfVBFT3U8mAWfBSHzNnoAQ8m+iLgU18mGbll9p6Rl",
	"2csm7EPgEz7A0QWre5HSXI9KQWQhsjjzFvIDlpIZz5hCpitYwpSi5SpGwFsMLmouv/fMcdfj5/eCCAgQ",
	"3VzMQZsRh1kxY/KUG01I4ErdJzGm7exQVkhwzHtWytxvvU9VigH6jKpLdVrlOS1XsXyuvMjAwUcyKz1i",
	"To+D+pg8Q78DRodYa7sLCTU/uUMCR6x5Po6YRK2beCuhEuzMdsFbxMP1MkL1bxXDPYdMi+dG6344HOQB",
	"Ue8jk5+GA8g0upiuIBvPsiuIFK6ND9YSxUWDYHg6YEnEb10WiGv5WFO/+/Hokc/mPi95po1CXnOfoeMl",
	"r47/8qJmJdH8AzmbKdZcaDQqoAbVxx1y8dSW9LpvR2FI6y67Ck6tfSveMl2VAo3DIIGA0Ewd9eRW3IAt",
	"7KIrtcMEAqTuR+C+IE5A/W3vFJoyrnmXIt7YgENiqHg5AkNhVQyG9S+LSqdyGWdr1iDwTIoZn1cldVJq",
	"c5NcveSl0m8rscEzwBVI9xxFfkNAZ+bDOnDMzkfKSgQxJj6ZC8QrSmZsSWbUkGI1JDaMXkgxgoxHo4Uk",
	"4XqByRgB1CnVPrR6yiA2JS+0IenmLb1gKytSi3uaTFlv0AnwEUyMS7fS/WAVuqRCzVhJnp4cQ06ICy0e",
	"94S2AIt9JRMa1w+ee5YE/M5wM3PTYC778XijgaM9S3t3w/CAY6hnT+2vtOQu/LeNIBd6KZc0wtveCDZa",
	"0hW5sh9jwDtkREqlIX5Umktuc+8gW4RD8lzJIKsyhwAkw3gnH40c/GliFUxeYrafE0kWkF+jnMfLpdX7",
	"IGfnKxuTs6WMrAnMo3bStJNn4aUfZpdfZFQbbWbkbTaY7wrigh1kuvKL7kM0+GizicSaVmtAuy+3OK+n",
	"VcqZaAYLW+uUVTDUOuLghlHrWN86stdGnw5jfE2LwsAYTtkdCjFbhhw67TPzOKa3Rza8+gtjxdtKiGjC",
	"fB0KtwwurnXa5XRFLhkrDFESTiiMi1B5Z57ugdaKQI9U3/B8xYhLK3CPNvWF2iTsNc6lxetjH9oHEvmC",
	"kcnSu9zYhFjfEqan1Bm0eH3MJADvuTT/FeyDbgShoWN7SCZNIEzI63enZ0ZDnkAy5GSreLMWID3U+mAU",
	"w3IfL3/sEh5aeq5NLlh/sVrh8JHhbz1/46ulWYAmxNLNHMVmSWyXHPGWzQ3bLllqPe8dSNI0LZlSO5YO",
	"sfQ3ftPkTC9pydZcw5093S4F6cKbqNVuMvZnFR+xDMCBKixA4gAxHCSYw3ph45M8FHpWHzutU5ZUJdcr",
	"nzvRooDbBtGvi54/ZboqnirFlaZCo/AZSzsJhTw5NbKd08FB7jKjED9Ml1pbQ9oLyEuhWyQm9yfifC1B",
	"rbuFKDxBnHvW66k4xWAha4yxrgdektOfnh48fITXXlX5kCj+D0j0na4gyNsIZLZ+AMnsolxCS9dq0jJ6",
	"wmzg5kXyM6hT3sdziULo4Ghw+HC6/+DJ/eTg8XT/8PAwvT+bPng4S/Yf//CE3j9I6P6j6f300YP99ODh",
	"oyePf9if/rD/OGUP9x+kj/cPnrB9MxD/Bxsc3X9w8AD8xDhbJudzLubhVI8Op48PkkeH0ycPDh7M0vuH",
	"0yeHj/dn00f7+4+e7P+wnxzS+w8f33+czA5p+uDBwaPDh9P7PzxOHtEfnjzcf/yknurg8aeuIcFB5CRK",
	"bc2vgfToFCHLr8MqBG4cV2jE+1asX6Vt4gIaTpVXitDnG4YfkWNBsDaJ9dUr51exY2EMkwttMw/O/XbI",
	"8fPzARqbnMrtAwZ8BhDFVYCuNrF2nJHKqvkeFKwYGeq1h0UfRsfPJz1ZrhZlttSmce0vecZOC5ZsVKxx",
	"8GHzmDbfppr7x+y65hla6VqnEqvCdA30sG7pNmKA4mxBX/vm9IIK6/VsRg5Q1RgU3DI2O5m6Uhz1NSZn",
	"gXTx+ci3RUDJlkfij7pL4KwKRp3URZHyWlplFx3Q4bik2HLky3o8NGXUI3pPbLT6Do2ssElqwzGjYwCd",
	"+dg1t7EmjR5sdNSY1djxhv3CbhPAv3K9qJ0wW4HaKeGJ81ZGQT+0YuqQpKywUfpAR5xP5Bs/m21lz+A4",
	"evw7nVMdrovD64wXWALqIMOqyCRNUR/D4KGoWQAHe4urgYo7LorzuoIHCBoN2PXKEjckNNyKgHAL7K3/",
	"8JvnhUnBca6GpwViNiVl8JljKcPwKK1tQjavOyuvjNzxkmcsiIACRDOcxL5mfnOJIbVcHyZk3xYO1BfT",
	"34ebQYtwIn/dvjCuBOT7c7EGK002CUfbS4znvyvP/VKEcC3RK1l6uklza7MSBZ/VHIumRii2Ol0QoUet",
	"VZWcV/v7B4+8PdhKZ5UymN8xNGtpB4zMhcKUvwdWgLqnmu6OaAZVYOHdwRLrDcOfhoMsANCOtpZbcJW0",
	"Tj2rNWS/9YYhpLmmKHbYLJnTarqmTOgpE2DF91mIGCKnIOR6TwXfTjA50xZx09IWb3JUMnjTPHwvpz4r",
	"kTxzY2LNqTnT4XNUvcDUS9WlT552f2dyrtCtJRizdTiKjCdcZys37ZRhFDk4Vsyj1dBvxGgRmH/j3jVj",
	"SIGxD99pCetpTD1zGbvv5fR74N3mdfPKPQX5nGC01jxn43PhfHxCajSNTFeQ3glaieUjVJOilFomMnOV",
	"kjy00DeDwPSlkCGzaVpKyHwyIzdjMpqXQxYbqUwEF944W/m2dfFig7hqQs7y1x9GjeUutGwewx6pRP2D",
	"oQzjnZNEZbGufN76rQdiol8GxEzVf0UlxD5QRIgD1eSSi9TmRGwNAx8ZlmU/yykEaWfZr96pZQszUHWZ",
	"yTk+DINjw9fP6Dzu/mpkIERrltUWraC4l5Y1NjYlmG1iXT4/JNA+OPz9/yP/9e+//8fv//n7//j9P/7r",
	"33//n7//5+//f5jLD1UlwrgPmAW0nqPBHgbu7qnZ3ns5VWjGuX9wOIaXwIxSicsLlGsOA5w8+eVHg6KF",
	"GhwZsQrqnBpp5/7o/j6WMryARDW2VL58JsQGY3lD9kEzYTN5xoV1DZmVXMhK+/JFjfXhFH6Fe/Gd2zqM",
	"nfFKKfXa8WxxTazqd1FzwkHGRfUhuH7gtR7Zo7KBz92I2xAJNsSK+IDXbSuob6gXEp71phgZ92pt+94q",
	"sqYOJ+yBWic8AGmNmBO1UprldcC3/bZVaQ/CDBM5F1yxrnhlX65jpinJ5JKVo4Qq5s2Wdgq3KBtico4H",
	"ej4YkvPBkotULhX+kdJyyQX+WxZMTFVq/mA6GZNTP5XMC6q5r4r+o7ynyKSsBPDBH9+8OZ38iZSVIBPw",
	"r8qMpFxpiPeDgAbDZakP/3MFif0i1fhcPFVO/qQZMTsaNvZBzl3Mz/nAGQdtcXe0zbhwbCjzWJSQD0EV",
	"OR80pU033vmghn0ulZEnQKy5ZEQzpfdSNq3mtnqkIowqDnUarTTi4kLRe80TksoE6vNCokuWNXYWLZvQ",
	"l4hifrjYvtTjkCSy4KGCOWkX/Bub0Sa+/G+3WOSZ/atO5jDEm6WEW/84FmJJJVPiniY51Qmmd9BEVzTz",
	"I3UM82dYdhhER9WuIQl4JLM0CKxrlotvl/D05cJdiZRzcdxYIFdE5sinhrWtDMqGrQqqVKtOdCedJwp0",
	"mw6u6RxFOXv7XDm4Ovo2SKM/fu5Dc2xNG8u7UX2kmviCm1NGDIlJqwyvv1kKGg0hPAGju2QZbMxgl8u+",
	"MmjovvAraaa/bSVFWfdrtx5OhMjF5Kx4C5AzV18Em35AfJtyGrQz17vqbkPCx2zsEi58mEwQJjXerbTG",
	"l2wcchNJkxiyezFdXbhopV2Cl22wQWStW6aw7VAxBNJotKwMnm7IV8ToNLHyJQPM/6V18oyNO9qtXMDX",
	"76tyU7majvTscuLb5ne2C5rEWrqEjVv8ZdrQw8WWPdqYoAhJctL2bwlKGX1WZau4d8IQGjCwt4oaDRsW",
	"9y6mBLWLNs5clVl84ndvX4VpyvXshGvFspn3ZMqlyCRNt4lAqksf+VPEnD/Yf9+pfEZmkU8kUHKmR+2E",
	"o5j+WE94l3KGwlt9jaShMC2kqxNXShPWzS6t0R3znWWj7nlddhDE3y7271i26S4Rw+umo29JkdxMfSe1",
	"rvIaPvMlHiHw3oly0lJpVMUQ86yZG+yNQLHgxKCMK4p62ATGSPb+9MB2JwsMGP4TkdZE0nqBzwVUKvgO",
	"5BvpIq4njt7aKmJCasJKaiNbfTmHttRulvX9pjJj3Rj1jAvbssNG30IkxT1FEt8XAgPMeZi+DeSavLli",
	"5bLkmqEsz2WloKCRCKpOuDzTqPgQK0L3Ss5tcTlPA7DOnZOKXTsJs2g4FZiQ0TLjPQW8dYME7kAloshV",
	"R3NG9YGSQVhKwkAnBOWdC4zKx3Eizv51gaCfRwXWXDI3aewS1XvcrmqJDRr1eXOdRIniIthjSzI4IfZZ",
	"p1LVWofMdgaV/rE+P7BV01hrnjOKlMLx/bpyGDRLyVk+RTzdSqRvVGvrLgC1q20GUJfbkdzgqBqupaD6",
	"TTSm9tNvw0gKfZcdOmpbo9mrbeqJdC/NrspRG0fXe4jd6P23A+O7A49BbfG2tmj7y8jXLotYURVLSgac",
	"Uo6E1CPNsmxExUoKFkYyHw0Oxwd9sD/6mwuYNZLbLC/Y3HbSGdWtVAbDQc5VEskEvWaouV34xy9/s9ry",
	"Gc7UdHTGprDI3H9kp3wu3rQPq1EA0Frm7QE+PTmGhnPBSVzUFbfUks7nrBxV/IYOplWasJvg0F+rq7Pa",
	"mz8mR0jiJ9NZ0ZpTyhgrTq3tK+KbNo+9bcyFJ6Aa6TLdTg3MwEXLRIppmF6+cXWkfNp4SldNPc2PbQg2",
	"KEpj8rQoMs5szUbMk5fmQw52q0lKV+pCzi6WjF1OINwP3mn+bl52takjKwSZUJCDB6OFrEry009Hr1/X",
	"WcTYk6hG23DkwdEgl0RXBOIowE2YXoDUfTS4/8PR/j4mrVilz6Y0A165t/afROukNCfpxkTShI0UK2iJ",
	"0bpLOcoYdIFy9XIs1KFIM10hX2TssgfM5LvzQS7R46Ar52z4fkxegLUzZ1Qocj5gV6xcmfFcVZxuX02/",
	"/0B0AoD2ZB450HyMF2L3gNo8XJvH+rGHTWg2xg1WvOZeaKpZn05tE8rLML1u+zSfqEYcDLbVotK+Aox0",
	"SS+vXYFxi4VuWF7T8uFLSg7tuoIylNB+xBwpU/YVOZsZZQSMA+26lzUC9Rf4jGT3Y6U6JFu14mmTHOuQ",
	"YCiqa8tJR2wD6iKj/1itDztq5k9a/wRqc2GHRiBXtYcFpZVaA7QKryIzLrha9PXUHH7B8xz6/a052T5r",
	"zJ+p4skawXP8GSWAl7uUAN7FiP5Vqu1+qQzBL1YLd5sKor4CT0uzKn1O7TXsTNuXuK31sZjiFyos5Ck6",
	"K6nwpqBsZeMoV07aoHPCdeC4h6osYNsYe9egNRMXRmCQs7oEv1E/ieLmbyoYGF+6UkJHI2vUZzRDp5L8",
	"ePKOYOCGt/K8ePHXFy/GdU3aH0/ejeC3iJDQ7Dq9cylNTedj8sz287XezFaJI2qr7aPh3qZcUHCzl1Sk",
	"MicwoDcRKcXnwlGqL2Q72aBbnNH5lqS/pvYeCVTHTmB3YBCheaKazi94CrrFg8P7B+mjH5IRo4/S0YOH",
	"jx6Nnkxnj0bsyWz/yZQ9+CFh04ha4UcIRP3NnUPWif5uxLXQcWp+ZzG7qvBRY8inNVOjkWQ7S1az/tPH",
	"6zqk4l1SIkaSM3SD+9MO2NQn1LIhLdmoQ3lo97igVSxB6J1iJRSQsAVzLcs4fj4kBVVqKcvUl1AGtdrW",
	"CTH6j7Nf1mYNg3oAGOBshq/WO11oXQw+fYLGi+jwgx4hiQ4MIJ5WnzGaW1cVfqmO9vZmLlyQy71ucQyM",
	"WSQvaZnbMFgImR4MBxlPmM3i8MTp1dVhZ/zlcjmei2osy/me/UbtzYtsdDjeHzMxXugciwlynTVWm/vS",
	"27Wyf3+8PwYFSRZM0IKDRcb8hHlIcDJ7tOB7V4d7Sbus0BwNJb4OxXEK7fh0s/4QyJiQAgKjHezvO6gy",
	"Ad9To4NiBPjee+tBQ7zdMgC+OR8cXhPowmB15lNREAWdoGVWjNEzzQz1WaczKV7qv0HQHxCgeowXIi0k",
	"t1W/57YzfWfATuVmA/koePcglGfPmVn6gP2Si/TPPqn8BDPHbgzc8b6YEXi/lJWoc8xBPfadSOFlG9j4",
	"hdaFxQ0i6zj1nQeXRuJfllLMx63Tf8ltxLssSS5LRp69OnZ9MNFZA3FviiwpRMyBDOW2E0OKQqrISUEC",
	"cuSogHf+WaarLwaNViGVCFhcB1BZWl8fRB5h8RCJQWRY+ubm8ahRmKG70l+aF3eIi8QwNzjSGRfs7uHU",
	"X2nGweFKQ2y6DjK18NR6ba/q8V0/8vogNxIVTFMaBYHAa1C2kXb1VbH25Nbw858CMTE7rcbIZvLaBna3",
	"wzi9yIipCVtKES8xe/uzjnyHwsWfho2xVjTPmmO15eJNCNI+iLfQY/eKxQWPrpyw9jSeJglTyvfejVRT",
	"jAxJwlQu3Ng98Om/KZh4enLsEtWyTC5texGINBc027OSpD3QCSlocmkO+1z0H7diuipG1NX36Sc7p/SK",
	"RUsK3QzhiU4VZZohWA3tpleI3i2kfBDp+NRCBohAX7IpLQpnJEmNijSrsqzu46ptpTEjV949UvKuDinq",
	"SW3FikPW6gRNbgTscEVmlUjwJkIh9g3obRAihtm9laP6cbDB+fY+umzTT3sfnRP20zqS1GCGzYblRgHn",
	"Bna2fINV4YJ81lpxto6qXVScbo6v0eIjEwbO5P4J29TrtxtkpvG87d0pptPSWknWWSPfO+zC1Mj0Nl9a",
	"k4BL9DbI6bO80fa/o363bjmN2uK9yd/9qOqToHbH0rrC539j6DU2oD4DOevKAG3zAXmn6oRnJ7TTNB0h",
	"M1mTBYdk1BcHZVPM+JpRaOliGEcseYRMqaqrN01LuVSNdLDrY3y9x91x3NXX7uH8kHyDLahuhNU3mpB1",
	"D/lnObX5yjnXHfS8SY1jzYLALVYZCQ95p80SM6KaDW8NmrQrgPaD+wc3LyOceYrq0+GYpnPImgOZsk6b",
	"a74QTZrj2Ps6W5G08tXJbAOjhCYLh3x+KLgPUpLMiCbn4lbFI3hAXEnMJiVAHLOeHagZKcvOHcG6DpBQ",
	"F8o+WCy+MdzPzRxCZi9l51Khar/F1QK99uveryRYwrrr9SCepr/jhfDZnoaKYh+OhREof3lzhtmVtrGe",
	"TV+o0/P0QlbzxX9fqD/KhQK02nCdAPv9vs1IYEqDEipLbk5c195ZHrlmjS5o/WZ5ppPFj5mc0kadCkgh",
	"u1kuEu8Zt5VAM4xfuTPXXc+lQ8PtoWIV7QjXIxdBHznIJmblle1WGvlcbTi+N1A1GLvj1FlIcwB0z3Ja",
	"55dTpUbYwAy36v7VPEDo9cZs47cbopa9beWits9mY7lmrXds6CZtY7bxtUmrwoZwIXHNKeSzmpviGpla",
	"ivjoVihiyXBNQgZt62pCaM9lfGeo1WtaXuJKQ5ANa2ncdTVJSq5ZyekGjIfxcnPbdhoUeYCTFuqEKyxg",
	"YJgCoIqjhLYqFRQyMydufs+bh94luTBoUUq0PS6Yf9envE9pcjkvZSXS8bn4RcJ8FO/spN2qcEK8qgph",
	"T+YrlpKqAFlJaF6Ca1+K1JUFySmiJ3rtOuDB+rkrWRH2oWCJHmJ1B8ZLMql7Tk3qRHZla+8aJS3DPVFo",
	"4gqztmybQEz+7nphxWUu6DRkyxndEAGx7bhiJrx2YdcmqZgzPb5tDafReqmfJQFUA8+KjRPDyhBQUYXP",
	"DDKDCAOkwDYngg/vDikAIcCXgDGA34671c2xZtCPCwLFREqUhADfLk8z4tveR/PfX2jO1pqGbIWUrQxD",
	"bsA7Y6dp13npVTHwWVsOsbkUXuA1MIVmNB4SG84nyPVvtnbGsjLRc1FbnIYa3CLQotYt/5LfjYoAMEBl",
	"2+QaVCogqVsDsZ7KMxQ/XheEHzHC7NNWstpWWO3rC/Tj9KYYuN+2EaeeIwkK6JhnTL6ujy75fG6k1dsl",
	"Wu8EckSWEsgM6PomMaAz4KSoAgwJF0lWpagcKatNQ58vow7IORYbRpXb1krygxh27YL0O+IB+UX6Bhuq",
	"0+X7uxXT3zcNlh6z+vWvr4oRt2Ia5KjbdZlOS0FyXcnXm5nwI5GSIIev7z7uTZsd8+M38y30WW3017/N",
	"A7kRiaveSkxhqQqDv99hzOnQ1sdYFex7I3MFbeO979LDcUtPsrubNElYAeWxmNAlZ9aoBWTFTnLXiAp0",
	"E3artfXIzZ0PQLDr/f46eHVzF30tcoEtZQ2CGdVqLjXCM6hBBbf/LqEC0igwATWT4evS8m4PgCaphGBa",
	"q+P6LavmDtdLHRgh41HNu+cccOJUbgdrX9v2hqa+bwEp/+AmxeZRX8O8GB200Yi8H4EU02G5oh7fDGgC",
	"J3VNoD84i3Q7sTm9Pa4OwZbEweaaJks3kc87osozRrRSHhz0leNyTTfdElwkHH7v42i/MtFcg6xeEqi3",
	"YMHQjHfZiKB1duQ69Dz1tav+2MjZKOHWg5rNBGOIzrBm5muh6WljuOsgaXNBFlPBc+UP22U1K9/Aw0v+",
	"fxA0bm5yFyQGPXQjez6Dt74Nngx78fl8cVkRYcyZCkupqY7kc8fEQmrXDQXgaJaFq25gwzbyXnzHcSRa",
	"LqgeLWWVpdY/OEplL055m9OvC6p/NR8d6+ffisDnPJJ9ch72SrBmnYgNwiBfIENhC0OXCe5sOpAIjaNA",
	"JIKrKu2iNbCW6BDsTJmc2yi4XnkMTEa240o9Sz0cGpagfqHw7q+UJFK4nIBs5abgKmitbb0Prlo9dkVE",
	"wVNWusco9WVgEeIqdsDZc83w9rAA7hqm3ewhe0PxPs1JYl6osGOci9EgtqHm7Tmfoj1AYzH+rg8mtM+2",
	"zToDdzjy6/0nN08s/UpoVjKarmwxcSswPLhV3zueHoSgiTkEspKJakG0bis3Ca4JojxPFkQKa96/NXZT",
	"tdhNi0g9wxa9tO6UitdfrfKMi0sfXQDdkhECGF+mkahYoFRGdMmywPqGfeCQWtgGWbbGe0KzzF/wOpKv",
	"ph8I1Hb2g10QJSq8TLCYRudmWjK6lmaEzf+2pRzhyd4oFYk1oNyWoHwFWhLtvxhbbzW1xwa9PSSI8+FB",
	"DMNaYuYd27DQulLu1JWB/p51c+QQBrZrLCb8FLLUyl78mvHajW1E+KeYcUZdtKJnG+0BfYs5FwGJfSpx",
	"FTXZgXeVNgKCX0L3lsCwex9dD9NPex/hF/6PNQ71sJ2hLJkLrW3JgFt3p4XiqV2B0b26kx9+2Jk3KBfv",
	"Gjv6SvGRWd3ut5m1blb8241fvE4Lyy0NkXfqEoVlzOpWm9Gmqw0BM7gv64i3x8h/bmQcxowqlqi4spnW",
	"52Bb36dsxkriO7m6XjuZzdg8Hxzs/3A+8IhVx9WBUgH+PV2Vwon09faUl+MwrNK3zu0cOEbi0UxJHEPJ",
	"nEnBCMsUjFPXL48tE7AFALhgFEsKWBD+PyOcZvSMitFzs8/ROxhgEIFh0KgzBkNZ8jkXNIM5zfjQugcL",
	"pGcyLKjuWwxzHfSrsi2CeUi1rZLnamAJQjm8AW2p5hxj0jft7Y1d2OilXdhgY6zSNvKMTDTTI6VLRvMm",
	"hfCa+pQLc7+HmxPDn+EcqtWX/Bp2RSeGdk2KB/s/bHrdomMDES3Jwfjex9ERSvu5UQcwDHfK9JJZZLfg",
	"DKKBvNZuw0Fmvq+6LDt0x4vODpdB2XkY6UKEl9ilTq+/te4G1jfHIp6LXZUzMmXmQz//dNW4dyhRTHqv",
	"0BExZzaxFQyBujSik285m2IDBwLOYPMp+vkOacbrNh7C/ZzJMuHTbEWSTNomDj+dnZ2QRAqBgeyuOZKE",
	"QpOW8Npqm6pxXoywDzTRRNGcWUlSS9dIjaSyMkIefqCgCS2+hamGeJvqWoOREyBTma56WWmY026mqLWL",
	"LlgakqN3nPQF+L2kZX5at2G5IcGonuUtiN7Xr4AVOg+4qiP0ZrTMNyTp49SdUVh7kAB+YJ3d+2h7/3xa",
	"b8CHcndbha36VkJ308BqWxZEHU9YklbM5B21zDebWq0xe0a+WHPye7ZjyvrTdz24vhUkcPtZhwvQVcvh",
	"Q09AWFvihA8XVBEBjWTIium7hU5hBEengRlGuucMszpw7xsciLaSTitsww053oB4Glozb4F8Z+bFu4N8",
	"mn3Qe0VGudixMtFZGzjfCl4FcWVUaTJjS9txKUAybGm/FfUKP/HjuS5Oa7Fqu6CKoCnTrWLVl7fgdlrj",
	"ffNxFcgCv4HACux45vPpwI3BZjOWaKcWQBdjHIEqsmRZ1s4uNN8yaiuFLKqcCoUx5CDcgwv+itNu9ZK6",
	"FLi5I9AYwN0oDAiFi1XfqwnhQmlG27l4QXn13pI4vhD6zUnhVs51U11bCPcCc6PBeV1KZr0cjqqx8g27",
	"sdOcM6FrWxrA54HSerqIhoPHMMrnek/TuTmJ+XbZOHVF620NGZrO68SYuxzBHrYsgBLvcBkqgcWuVaNd",
	"tQ/zN7tD34gZQ0FpgfoYazBvCHlfA9Yvh8hBNfI4GQ82H0FhL/SHr/XudRu+N/8CbK+oIjDFEnZNoH55",
	"7rgRnjYbuQWwaxoEDabZbp/+OmGFk7uTGWtLB1KBUQ1QZ3AbZGkg2tBuE9q82HR22sTNPkK2IVbQH5i6",
	"lWv2qiffo27Er8ZrsjGX4Wv99yxe4ReCIL76BdgN8W+R0pnLFIQCoT3ZxQVBkxPlXT5DomRtL01olllD",
	"6aWQSwhje/fu+PnduYQ+AEaw5a7XDyWRJurFb1vQzXLThbuF29Z31f4CXhC31k13TW0FI5tM4j51om7D",
	"4RJrA9AF3t5H2xtjB9FrK5XSD3vz6dCdetkWdzyPsrGQd1Pic9rS0vZhPNZ48xOZ575pM/iAEwhZBgeU",
	"rXFbG1CWvg0OF2RiW7BNQLlCD2rzJQxZsf2fhoaJF4RrMuOl0mPyVKzQIoOvha1WgmGczxXIeuV7nF1P",
	"7vyqOPWlScEajrttWvXS913bRl4hKdMU6tQt62l2uPnbWJWszt9tRnbbR3dTQkS0wdpdMDbdETtQLwJu",
	"Zw1yGL0TUjqButfQ2ZCnvwk07DRF68HBroxOjp+rhgmh9lu7HupEzv45cTSoKG8ghdBQC154C9ivu+Nn",
	"xlgxUkHX5U1crtmm+Vtiec2dbdPUBLz5jb7U65K6WSjUCRn78m6i4AbK9VUx4sY46SZkcDna7VO8tmXK",
	"98X+qnapa9ImI8DJ0lnWGv2EI2jecmNg70FWjvDvdfIbvujl7Zs7/7dBP8R11idJ3Opv1TTjIMHSfnG9",
	"4065OzF2bvkN80pHUejIaPWRGJZXf6kiSGX0vZGczdaIXnwu3sxmW7lg7h4sbYdQILGN3qB/g3ajrRKp",
	"gc5LFanbm68F+DOaZRjt6awzWpLMuuFcmVMw3+kFW90rGZlDKRo7/Lj3VMSGQxE3erXtFP2XOmeaplTT",
	"r2BsDZv9/yGu9NZo+LTSCyY0ZBW4Pn0GG1woap+14LNxEgO5tYQZbA6zDDgVrw88irHaJhJHBePg1AZf",
	"GzlgpU678UEcvQKpkKT/i7uNVbtjiMuQc039WYlZJ2LVA4ReVBjhm2k/CescVjq4aZuPnyimtdT+C+Xx",
	"dGcJ9Q9MeSxVt+fm7MkQlpB444IiNDFkI2Mp1nbExDNLUUbNmCiHLuBb5aJOeLJUhpWjTCY0AwJHM/Wl",
	"qdoVa+ymirmXIDhoDZ+18riNG7+5+rrW8N4b1g3l6oJ2L33k6hfp6qn6tFZfZCywezzYP/yCrQ8RxXoR",
	"84SVrvPMcyY4kk5b/yBuOscQOsvyaKL5FVpiGbhHXY2tLJNL9FVYsNitl3y+0ETIpQ3gO7xdBuMuEhWQ",
	"04cOPCOFw+owMw8y/ucSWtrbzBa8cDteWusepH78ABqbbhPglFM4y3hToGgEXf91MUOi/e1bCEa1O+m7",
	"jlY24gKX6AIDr2XVsGN1o09jt6TO8VANj53DJFfWU0mbD+fHrkvT3bbB5DOZU8Ooqy6HRK8KnkDsoe3W",
	"BAJzUcp5yZQaQjsn1+BClmRGeVaVbCOHcXxFMZE2HHUG3G50qL7NSrb5puzldDXio7LqDyt9TVfWlFKJ",
	"byIp5TVd/YWx4i16nL8x9QwDv60YU2d/BxJz4HoPGFRZCbJHLhkrnCu+DgAnbwpXOwoSESkXilCCrvZQ",
	"JvVOmZj/vQeROxI9KHvBylpr4qqOSl+P2rLSRaVHRSnTKlkn6Bti+QZePnHv3gnmADW/9t4XbL5rNvbQ",
	"fluI+ddK5D7YMpEbpD+bouzafjy4f//mL9orJuZ64Ysf/SnsHJfyFPuFGypLiQXByH6Cefl2pYc3v9IT",
	"uoJ8XWhbR0vb7+vB/Ye34UZQVVHI0hzUa5ZySs5WhfWYAYoRxCgnTE59unndBTaM/npw8OR2Ogy6+hfI",
	"KYF0SIkdpmbmYttCe9YtrRel1DpjthzfH0rywDx3A+hcKk1KlmD2vy8dCPtFeSDIducAHOw7ZT6uHSFM",
	"KKz9hzkUIL3bUzZf3lMk5XOmoHhw+4zJM199AOLETn75EeD888mLH4lFJTNokVEh4nFa6wQevajyqaA8",
	"U3tFya44WzqyxEssmOioPUHq78QggGh55ah5VWaDo8HeIDBCtYnVcTMIqtMWzGGKZweQpNItJPKznDoz",
	"Kchof69YyQ361e1Oh612FONGFU0VGfTpyXGzP2RoIpN5XgkUN6FASXvp47YDNzKBxYbXfk3k6cnxsL87",
	"MzazMtswd6WUmVtRZzJwOkZK5WD5AT8L8Im6doKFoO9Z+V5OfUW4cA5b7uDTb5/+TwAAAP//ah7ySOEQ",
	"AQA=",
}

// GetSwagger returns the content of the embedded swagger specification file
// or error if failed to decode
func decodeSpec() ([]byte, error) {
	zipped, err := base64.StdEncoding.DecodeString(strings.Join(swaggerSpec, ""))
	if err != nil {
		return nil, fmt.Errorf("error base64 decoding spec: %s", err)
	}
	zr, err := gzip.NewReader(bytes.NewReader(zipped))
	if err != nil {
		return nil, fmt.Errorf("error decompressing spec: %s", err)
	}
	var buf bytes.Buffer
	_, err = buf.ReadFrom(zr)
	if err != nil {
		return nil, fmt.Errorf("error decompressing spec: %s", err)
	}

	return buf.Bytes(), nil
}

var rawSpec = decodeSpecCached()

// a naive cached of a decoded swagger spec
func decodeSpecCached() func() ([]byte, error) {
	data, err := decodeSpec()
	return func() ([]byte, error) {
		return data, err
	}
}

// Constructs a synthetic filesystem for resolving external references when loading openapi specifications.
func PathToRawSpec(pathToFile string) map[string]func() ([]byte, error) {
	var res = make(map[string]func() ([]byte, error))
	if len(pathToFile) > 0 {
		res[pathToFile] = rawSpec
	}

	return res
}

// GetSwagger returns the Swagger specification corresponding to the generated code
// in this file. The external references of Swagger specification are resolved.
// The logic of resolving external references is tightly connected to "import-mapping" feature.
// Externally referenced files must be embedded in the corresponding golang packages.
// Urls can be supported but this task was out of the scope.
func GetSwagger() (swagger *openapi3.T, err error) {
	var resolvePath = PathToRawSpec("")

	loader := openapi3.NewLoader()
	loader.IsExternalRefsAllowed = true
	loader.ReadFromURIFunc = func(loader *openapi3.Loader, url *url.URL) ([]byte, error) {
		var pathToFile = url.String()
		pathToFile = path.Clean(pathToFile)
		getSpec, ok := resolvePath[pathToFile]
		if !ok {
			err1 := fmt.Errorf("path not found: %s", pathToFile)
			return nil, err1
		}
		return getSpec()
	}
	var specData []byte
	specData, err = rawSpec()
	if err != nil {
		return
	}
	swagger, err = loader.LoadFromData(specData)
	if err != nil {
		return
	}
	return
}
